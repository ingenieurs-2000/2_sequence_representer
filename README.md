# Industrie 4.0 : Séquence Représenter

*Ingénieur 2000 / Référent Anthony Baillard*

![](https://i.imgur.com/Clu8I97.png)

*&copy; [Julien Noyer](https://www.linkedin.com/in/julien-n-21219b28/) for [Ingénieurs2000](https://www.ingenieurs2000.com) - All rights reserved for educational purposes only*

---

<br>

# Définition du répertoire

Le contenu de ce répertoire à pour objectif de servir de base pour la conversion de données et réalisation de datavisualisation sur une page Web.

- **Supports de cours** https://hackmd.io/@school-inge-2000/BkEG3IDVu
- **Lien vers la version finale** https://dataviz.dwsapp.io

<br>

## Utilisation du répertoire

LE projet contenu dans ce répertoire utilise des fonctionnalités asynchrones qui ne peuvent fonctionner que dans un environnement serveur, c'est pourquoi nous vous conseillons d'installer **LiveServer** sur votre machine avec la commande suivante : 

```
sudo npm i -g live-server
```

Il suffi ensuite de vous placer dans le dossier dans lequel vous souhaitez lancer votre environnement serveur et taper la commande suivante : 

```
live-server
```

---

<br><br><br>

# Notation de la séquence

![](https://i.imgur.com/FC9fnu5.png)

<br>

En plus du formulaire général sur les notions en Data aborder pendant la séquence "**Représenter**", vous aurez à réaliser un **exercice noté sur 20** dans lequel vous devrez mettre en pratique les techniques abordées lors des sessions de cours avec Julien Noyer.

<br>

## Enoncé de l'exercice

Lors des séquences vous aurez édituer le fichier `index.html` pour y intégrer des fonctionnalités qui permettent de charger des fichiers CSV et d'organiser les données chargées pour pourvoir les visaluser dans des graphiques.

Vous devez créer un nouveau fichier HTML nommé `exerciceNOMprenom.html` dans lequel vous devez intégrer les CDN nécessaires à l'utilisation de D3js et ChartJS. Vous devez ensuite charger les fichiers suivants pour faire en sorte d'afficher un grafique à barres et un graphique doughnut qui représentent les données chargées depuis les CSV.

- https://gitlab.com/ingenieurs-2000/2_sequence_representer/-/blob/master/data/meteo.zip

<br>

Les données à traiter sont celles de la météo dans le ville de Seattle de 2012 à 2015, elles intègrent différents formats et la définition du type de climat par jour. Vous devez combiner les données afin de réaliser les graphiques suivants : 


- **Graphique doughnut** : nombre de jours par type de climat sur toute la période
- **Graphique à barres** : nombre de jours par type de climat et par mois

<br>

## Restitution

Pour restituer votre travail vous devez réaliser un fichier **ZIP** de votre dossier de travail afin que votre correcteur puisse tester le code dans son environnement local.

<br>

Pour créer une archive ZIP sur Mac, il vous suffit de faire un click-droit sur votre dossier de travail et de sélectionner l'option "Compresser...". Pour créer une archive ZIP sur Windows, il vous suffit de faire un click-droit sur votre dossier de travail et de sélectionner l'option "Envoyer vers > Ficher compressé".

- Créer un fichier **ZIP sur Mac** : https://apple.co/30W1cul
- Créer un fichier **ZIP sur Windows** : https://bit.ly/3sgc3vf

<br>

Une fois votre archive créée, il est essentiel que vous respectiez la nomenclature suivante pour nommé votre archive :

- **NOM_PRENOM_JS_REPRESENTER**

<br>

Vous devez ensuite effectuer votre rendu via le LMS Ingénieur2000 en y déposant votre fichier **ZIP** dans le dossier correspondant à la séquence.

--- 

<br><br><br>

# Ressources

*Liste de liens utiles pour la mise en place de la séquence*

![](https://i.imgur.com/eAySYs0.png)

- MDN web docs https://developer.mozilla.org/fr/docs/Web/JavaScript
- D3.js https://d3js.org
- Vega-Lite https://vega.github.io/vega-lite-v1/
- Learn JS Data http://learnjsdata.com

<br>

---

*&copy; [Julien Noyer](https://www.linkedin.com/in/julien-n-21219b28/) for [Ingénieurs2000](https://www.ingenieurs2000.com) - All rights reserved for educational purposes only*

---